# Sonicast

## Introduction
Sonicast is a sonic weather web application made up of 3 parts:
* The fontend is based on AngularJs, JQuery and PHP. It collects data and sends it to the weather API in order to send the weather forecast back to the website
* The backend is in ASP.net core C# responsible for listing cities with their spatial coordinates (longitude and height).
* A "Meteormatics" weather API to give the weather forecast for a given city.

## I - Dockerization of the application
To ensure the scalability aspect of the application, the first step is to dockerise the application first, you have to set up:
* a container for the frontend: a php: 7.4-apache image using the files of the sonicast application
* a container for the backend: an image mcr.microsoft.com/dotnet/core/aspnet:3.1-buster-slim for the C # files of the sonicast application

### II - Deployment of the application
#### Project cloning
``` bash
cd $ HOME
git clone https://gitlab.com/iamtah3r/sonicast-jenkins.git
cd sonicast-jenkins
```
#### Creation of the "sonicast-frontend" image, "sonicast-backtend" image and jenkins installation via docker-compose
``` console
cd sonicast-jenkins
docker-compose up
```
## III - Pipeline
Creation of a continuous integration chain for the Sonicast application.
The static analysis of a PHP project by a PIC such as Jenkins requires an optimization of the rules for excluding directories that are not part of the developed code. You have some tips to speed up the continuous integration of a PHP project.
These utilities can be used outside of the PIC to check this or that aspect. There are other PHP code analysis utilities like PhpCheckstyle or Yasca (security audit). The purpose of this post is not to talk about the other tools that can be integrated into the PIC (phpunit, selenium…), but about the optimization of the aforementioned control tools which turn out to be very greedy in system resources.
The pipeline is made up of 12 stages:
##### 1 - Checkout
Copy the entire project code from version control.
##### 2 - Preparation + Composing
Installation and download of dependencies to create a test environment.
##### 3 - PHP check Syntax
** parallel-lint ** to check PHP syntax in all project files
##### 4 - Tests
** Unit testing ** is a method of software testing by which individual units of source code - sets of one or more computer program modules.
** The acceptance test ** consists of evaluating whether the product works for the user, correctly for the use.
** Code coverage ** is a metric that can help you understand how tested your source is.
##### 5 - Style check
** phpcs ** for code adherence to certain coding conventions.
This tool checks the input PHP source code and reports any deviations from the coding convention.
##### 6 - Lines of code
** phploc ** to quickly measure the size and structure of a PHP project
##### 7 - Copy paste detection
** phpcpd ** for duplicate code detection (by copy / paste).
##### 8 - Disorder detection
** phpmd ** for the detection of certain bugs, parameters, methods or properties not used, etc.
It finds common programming flaws such as unused variables, empty catch blocks, creation of unnecessary objects, etc.

##### 9 - Software metrics
** pdepend ** for statistics on the quality of the code (cyclomatic complexity, depth of inheritance, number of overloaded methods…). This utility is based on JDepend.
This tool extracts information, particularly on maintainability, complexity, cohesion, etc.
##### 10 - Construction of the image
Set up a docker image from the tested code
##### 11 - Deployment of the image
Upload the image to the Artifact repository on Gitlab
##### 12 - Removal of the used docker image
Household: Delete the image locally.

## IV - Personal feedback on the project
This project allowed me to use the knowledge acquired during the training. I dug into continuous integration (CI) which consists of automating the integration of code changes from several contributors into a single software project. This is a core DevOps best practice, allowing developers to frequently merge code changes into a central repository where builds and tests then run. Automated tools are used to assert the accuracy of the new code prior to integration.